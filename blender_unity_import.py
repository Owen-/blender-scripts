import bpy
import math
from mathutils import Matrix

# Bones
# Armatures
# Ensure armatures parented to curves etc ok
# animations with and without armatures
# tags instead of weird names - debug, selection and rotation modes
# performance
# there could totally be issues making a new object
    # really need to think about that


debug_mode = False
no_rotation = False
selection_mode = True
mrot90 = Matrix.Rotation(-math.pi / 2.0, 4, 'X')
mrot90n = Matrix.Rotation(math.pi /2.0 , 4, 'X')
    
def setObjAxis(ob):
     #if (ob.type== 'ARMATURE'):
        #for bone in ob.data.bones:
        #    mm = mrot90*setAxis(bone.matrix.to_4x4())
        #    bone.matrix = mm.to_3x3()
        #    nm = mrot90*bone.matrix_local.copy()
        #    bone.matrix_local = setAxis(nm)
     ob.matrix_basis = setAxis(ob.matrix_basis.copy())
     ob.matrix_parent_inverse = setAxis(ob.matrix_parent_inverse.copy())
    
def setAxis(mat):
    loc, rot, scale = mat.decompose()

    erot = rot.to_euler('XYZ')
    erot [1], erot[2] = erot[2], erot[1]
    loc.rotate(mrot90)
    scale[1], scale[2] = scale[2], scale[1]

    smat = Matrix()
    for i in range(3):
        smat[i][i] = scale[i]
        
    return Matrix.Translation(loc) * erot.to_matrix().to_4x4() * smat

def convertToMesh(ob):
    mesh_data = ob.to_mesh(s, True, 'PREVIEW', False, False)
    mesh_data.transform(mrot90)
    try:
        ob.data = mesh_data
    except:
        n = ob.name
        n = "new obj"
        m = ob.matrix_world.copy()
        s.objects.unlink(ob)
        bpy.data.objects.remove(ob)          
        ob = bpy.data.objects.new(n, mesh_data)
        ob.matrix_world = m
        s.objects.link(ob)
        
def modify_armature(ob):
    #ob.
    for pb in ob.pose.bones:
        pb.matrix_basis = setAxis(pb.matrix_basis.copy())
        #pb.rotation_euler.xyz = pb.rotation_euler.xzy
        #pb.bone.head.xyz = pb.bone.head.xzy
        #pb.bone.tail.xyz = pb.bone.tail.xzy
        #pb.matrix_channel = setAxis(pb.matrix_channel.copy())
        #pb.matrix = setAxis(pb.matrix.copy())
    #ob.pose.bones.update()
    
if not no_rotation:
    for s in bpy.data.scenes:
        for ob in s.objects:
            #modify_armature(ob)
            try: modify_armature(ob)
            except: pass
        
            try: convertToMesh(ob)      
            except: pass
           
            setObjAxis(ob)
            
            
            
            
for s in bpy.data.scenes:
    if s.name == "__BLENDER_UNITY_IMPORT_DEBUG":
        debug_mode = True
    if s.name == "__BLENDER_UNITY_OLDROTATION":
        no_rotation = True
    if s.name == "__BLENDER_UNITY_KEEPHIDDEN":
        selection_mode = False
            
if bpy.path.basename(bpy.context.blend_data.filepath).endswith("__USEOLDROTATION.blend"):
    no_rotation = True
            
if debug_mode:
    debug_path = bpy.path.abspath("//")
    print("debug mode active, but invisible in real file")
    
    
    
        #for a in bpy.data.armatures:
        #    pass
        
        #for ob in s.objects:
        #    if ob.type == 'ARMATURE':
        #        #temporary fix until i figure out how to set the bones properly
        #        # why isnt the armature thing getting applied
        #        ob.matrix_basis = mrot90 * ob.matrix_basis
        #        for c in ob.children:
        #            c.matrix_local = mrot90n * c.matrix_local